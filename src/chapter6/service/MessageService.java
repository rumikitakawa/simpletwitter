package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {
		//つぶやきの追加
		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserMessage> select(String userId,String start, String end) {
		final int LIMIT_NUM = 1000;
		String startDate;
		String endDate;

		if(!StringUtils.isEmpty(start)) {
			startDate = start + " 00:00:00";
		} else {
			startDate = "2020/01/01 00:00:00";
		}

		if(!StringUtils.isEmpty(end)) {
			endDate = end + " 23:59:59";
		} else {
			SimpleDateFormat format = new SimpleDateFormat( "yyyy/MM/dd HH:mm:ss" );
			endDate = format.format(new Date());
		}

		Connection connection = null;
		try {
			connection = getConnection();
			// idをnullで初期化
			Integer id = null;
			//ServletからuserIdの値が渡ってきていたら整数型に型変換し、idに代入
			if(!StringUtils.isEmpty(userId)) {
				id = Integer.parseInt(userId);
			}

			//messageDao.selectに引数としてInteger型のidを追加
			//idがnullだったら全件取得する、 idがnull以外だったらその値に対応するユーザーIDの投稿を取得する
			List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, startDate, endDate);
			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(String messageId) {
		//つぶやきの削除
		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().delete(connection, messageId);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public Message edit(String messageId) {
		//つぶやきの編集
		Connection connection = null;
		try {
			connection = getConnection();
			Integer editMessageId = Integer.parseInt(messageId);
			Message message = new MessageDao().edit(connection, editMessageId);
			commit(connection);

			return message;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update(Message message) {
		//更新
		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().update(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}